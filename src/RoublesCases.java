public class RoublesCases {
    public static void main(String[] args) {
        int roubles = 102;

        int hundredRemainder = roubles % 100;
        int tenRemainder = roubles % 10;

        String cases;
        if (tenRemainder == 1 && hundredRemainder != 11) {
            cases = " рубль";
        } else if (tenRemainder >= 2 && tenRemainder <= 4 && (hundredRemainder != 12 && hundredRemainder != 13 && hundredRemainder != 14)) {
            cases = " рубля";
        } else {
            cases = " рублей";
        }
        System.out.println(roubles + cases);
    }
}
