public class DoubleString {
    public static void main(String[] args) {
        String str1 = new String("Далее выведите на экран количество символов в данной строке.");
        System.out.println(str1.length());

        String str2 = str1.substring(0, str1.length() / 2);
        String str3 = str1.substring(str1.length() / 2, str1.length());
        System.out.println("Первая часть строки: " + str2);
        System.out.println("Вторая часть строки: " + str3);

    }

}