public class TheNextDate {
    public static void main(String[] args) {
        int day = 31;
        int month = 12;
        int year = 2019;

        if (((day >= 1) && (day <= 29)) && ((month == 4) || (month == 6) || (month == 9) || (month == 11))) {
            day++;
        }
        if (day == 30 && (month == 4 || month == 6 || month == 9 || month == 11)) {
            day = 1;
            month++;

        }
        if (day == 31 && (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10)) {
            day = 1;
            month++;
        }
        if (day == 31 && month == 12) {
            day = 1;
            month = 1;
            year++;
        } else if (month == 2 && day == 28) {
            day = 1;
            month++;
        } else if (month == 2 && day == 29 && (year % 400 == 0 || year % 4 == 0)) {
            day = 1;
            month++;

        }
        System.out.println("Следующий день: " + day + "  " + month + "  " + year);
    }
}