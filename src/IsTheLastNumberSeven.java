public class IsTheLastNumberSeven {
    public static void main(String[] args) {
        int number = 1234568;
        int remainder = number % 10;
        boolean isSeven = remainder == 7;
        if(isSeven)
            System.out.println("The last number is seven!");
        else
            System.out.println("The last number isn't seven!");
    }
}
